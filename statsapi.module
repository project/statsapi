<?php

/*
 * @file
 *   Collects statistics about modules.
 */

/**
 * Implementation of hook_menu().
 */
function statsapi_menu() {
  $items['stats'] = array(
    'title' => 'Site Stats',
    'page callback' => 'theme',
    'page arguments' => array('statsapi_page'),
    'access arguments' => array('view statsapi'),
  );
  return $items;
}

/**
 * Implementation of hook_perm().
 */
function statsapi_perm() {
  return array('view statsapi');
}

/**
 * Implementation of hook_cron().
 */
function statsapi_cron() {
  statsapi_compute();
  watchdog('statsapi', 'stats computed');
}

/**
 * Implementation of hook_theme().
 */
function statsapi_theme() {
  return array(
    'statsapi_page' => array(
      'arguments' => array(),
    ),
  );
}

/**
 * Renders the stats page.
 */
function theme_statsapi_page() {
  $values = db_fetch_array(db_query_range("SELECT * FROM {statsapi} ORDER BY saved_time DESC", 0, 1));
  $data = unserialize($values['data']);
  $time = format_date($values['saved_time']);
  $output = '<p>'. $time .'</p>'. drupal_get_form('statsapi_recompute');
  foreach ($data as $key => $value) {
    $output .= '<h3>'. check_plain($key) .'</h3><p>';
    foreach ($value as $key => $val) {
      $output .= '<strong>'. t($key) .': </strong>'. filter_xss_admin($val) .'<br />';
    }
    $output .= '</p>';
  }
  return $output;
}

/**
 * Recompute button.
 */
function statsapi_recompute($form_state) {
  $form['recompute'] = array(
    '#type' => 'submit',
    '#value' => t('Recompute Stats'),
  );
  return $form;
}

/**
 * Submit function for the recompute button form.
 */
function statsapi_recompute_submit($form, &$form_state) {
  statsapi_compute();
}

/**
 * Computes stats.
 */
function statsapi_compute() {
  $count = 0;
  $stats = array();
  foreach (module_invoke_all('compute_statsapi') as $module => $implementation) {
    $stats[$module] = $implementation;
  }
  $object = array('saved_time' => time(), 'data' => serialize($stats));
  drupal_write_record('statsapi', $object);
}

/**
 * Implementation of hook_compute_statsapi().
 *
 * Returns an array of arrays. Outer keys are the names of modules for which the
 * inner arrays hold statistics. Keys of the inner arrays are the title of the
 * statitic and values of the inner arrays are the values of the statistics.
 */
function statsapi_compute_statsapi() {
  //Nodes.
  $nodecount = db_result(db_query("SELECT COUNT(*) FROM {node}"));
  /*
  $types = node_get_types();
  $nodecounts = array();
  $nodeusers = array();
  foreach ($types as $type) {
    $nodecounts[] = t('@type: @value', array('@type' => $type, '@value' => db_result(db_query("SELECT COUNT(*) FROM {node} WHERE type = '%s'", $type))));
    $nodeusers[] = t('@type: @value', array('@type' => $type, '@value' => db_result(db_query("SELECT COUNT(DISTINCT(uid)) FROM {node} WHERE type = '%s'", $type))));
  }
  */
  $nodetotals = $nodecount/* . theme('item_list', $nodecounts)*/;

  //Node revisions.
  $revisions = db_result(db_query("SELECT COUNT(vid) FROM {node_revisions}"));
  $noderevisions = t('@revisions (extra: @extra)', array('@revisions' => $revisions, '@extra' => $nodecount - $revisions));

  //Users.
  $usercount = db_result(db_query("SELECT COUNT(*) FROM {users}"));
  $xusercount = db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE status <> 1 && uid <> 0"));
  $latestuser = db_fetch_object(db_query_range("SELECT * FROM {users} ORDER BY created DESC", 0, 1));
  $latestuserstat = t('!user registered %time ago', array('!user' => theme('username', $latestuser), '%time' => format_interval(time() - $latestuser->created)));
  $contentusers = db_result(db_query("SELECT COUNT(DISTINCT(uid)) FROM {node}"));
  $contentusertotals = $contentusers/* . theme('item_list', $nodeusers)*/;
  $rolecount = db_result(db_query("SELECT COUNT(*) FROM {role}"));
  $result = db_query("SELECT COUNT(u.uid) count, r.name FROM {users_roles} u INNER JOIN {role} r ON u.rid = r.rid GROUP BY u.rid ORDER BY u.rid ASC");
  $rolecounts = array();
  while ($role = db_fetch_object($result)) {
    $rolecounts[] = t('@type: @value', array('@type' => $role->name, '@value' => $role->count));
  }
  $rolecounts = theme('item_list', $rolecounts);

  //Blocks.
  $blockcount = db_result(db_query("SELECT COUNT(*) FROM {blocks}"));
  $result = db_query("SELECT theme, COUNT(bid) count FROM {blocks} GROUP BY theme");
  $blockcounts = array();
  while ($theme = db_fetch_object($result)) {
    $blockcounts[] = t('@type: @value', array('@type' => $theme->theme, '@value' => $theme->count));
  }
  $blocktotals = $blockcount . theme('item_list', $blockcounts);
  $blockcounte = db_result(db_query("SELECT COUNT(*) FROM {blocks} WHERE status = 1"));
  $result = db_query("SELECT theme, COUNT(bid) count FROM {blocks} WHERE status = 1 GROUP BY theme");
  $blockcountes = array();
  while ($theme = db_fetch_object($result)) {
    $blockcountes[] = t('@type: @value', array('@type' => $theme->theme, '@value' => $theme->count));
  }
  $blocketotals = $blockcounte . theme('item_list', $blockcountes);

  //Return stats.
  $return = array();
  $return['block'] = array(
    'Number of blocks' => $blocktotals,
    'Number of enabled blocks' => $blocketotals,
    'Number of custom blocks' => db_result(db_query("SELECT COUNT(*) FROM {boxes}")),
  );
  if (module_exists('comment')) {
    $result = db_query("SELECT COUNT(c.nid) count, n.type FROM {comments} c INNER JOIN {node} n ON c.nid = n.nid GROUP BY n.type");
    $nodecommented = array();
    while ($value = db_fetch_object($result)) {
      $nodecommented[] = t('@type: @value', array('@type' => $value->type, '@value' => $value->count));
    }
    $commented = db_result(db_query("SELECT COUNT(DISTINCT(nid)) FROM {comments}"));
    $commentcount = db_result(db_query("SELECT COUNT(nid) FROM {comments}"));
    $commenttotals = $commentcount . theme('item_list', $nodecommented);
    $return['comment'] = array(
      'Number of comments' => $commenttotals,
      'Number of comments today' => db_result(db_query("SELECT COUNT(cid) FROM {comments} WHERE timestamp > (%d - (60 * 60 * 24))", time())),
      'Average number of comments per day this week' => round(db_result(db_query("SELECT COUNT(cid) FROM {comments} WHERE timestamp > (%d - (60 * 60 * 24 * 7))", time())) / 7, 1),
      'Average number of comments per day since the site was started' => round($commentcount / ((time() - db_result(db_query("SELECT created FROM {users} WHERE uid = 1"))) / (60 * 60 * 24)), 1),
      'Nodes with comments' => $commented,
      'Nodes with no comments' => $nodecount - $commented,
    );
    $return['user']['Number of users who posted comments'] = db_result(db_query("SELECT COUNT(DISTINCT(uid)) FROM {comments} WHERE uid != 0"));
  }
  if (module_exists('menu')) {
    $return['menu'] = array(
      'Number of menus' => db_result(db_query("SELECT COUNT(*) FROM {menu_custom}")),
    );
  }
  $return['node'] = array(
    'Number of nodes' => $nodetotals,
    'Number of nodes today' => db_result(db_query("SELECT COUNT(nid) FROM {node} WHERE created > (%d - (60 * 60 * 24))", time())),
    'Number of revisions' => $noderevisions,
    'Average number of nodes per day since the site was started' => round($nodecount / ((time() - db_result(db_query("SELECT created FROM {users} WHERE uid = 1"))) / (60 * 60 * 24)), 1),
  );
  if (module_exists('path')) {
    $return['path'] = array(
      'Number of aliases' => db_result(db_query("SELECT COUNT(*) FROM {url_alias}")),
    );
  }
/*
  if (module_exists('statistics')) {
    $result = db_query_range("SELECT nid, totalcount FROM node_counter ORDER BY totalcount DESC, daycount DESC", 0, 5);
    $nodes = array();
    while ($n = db_fetch_object($result)) {
      $node = node_load(array('nid' => $n->nid));
      $nodes[] = t('@node (@count)', array('@node' => l($node->title, 'node/'. $node->nid), '@count' => $n->totalcount));
    }
    $popularnodes = theme('node_list', $nodes);
    $return['statistics'] = array(
      'Total node views' => db_result(db_query("SELECT SUM(totalcount) FROM {node_counter}")),
      'Popular nodes' => $popularnodes,
    );
  }
 */
  $return['statsapi'] = array(
    'Number of times stats were calculated' => db_result(db_query("SELECT COUNT(*) FROM {statsapi")),
  );
  $return['system'] = array(
    'Number of modules installed' => db_result(db_query("SELECT COUNT(*) FROM {system} WHERE status = 1")),
  );
  if (module_exists('taxonomy')) {
    $termcount = db_result(db_query("SELECT COUNT(*) FROM {term_data}"));
    $vocabcount = db_result(db_query("SELECT COUNT(*) FROM {vocabulary}"));
    $return['taxonomy'] = array(
      'Number of terms' => $termcount,
      'Number of vocabularies' => $vocabcount,
    );
  }
  if (module_exists('upload')) {
    $return['upload'] = array(
      'Number of uploads' => db_result(db_query("SELECT COUNT(*) FROM {upload}")),
    );
  }
  $return['user'] = array(
    'Number of users' => $usercount,
    'Number of users logged in today' => db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE access > (%d - (60 * 60 * 24))", time())),
    'Number of users logged in this week' => db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE access > (%d - (60 * 60 * 24 * 7))", time())),
    'Number of users who have returned to the site' => db_result(db_query("SELECT COUNT(uid) FROM {users} WHERE access > created + 24 * 60 * 60")),
    'Active user accounts' => $usercount - $xusercount,
    'User accounts disabled or never activated' => $xusercount,
    'Newest user' => $latestuserstat,
    'Number of users who posted nodes' => $contentusertotals,
    'Number of roles' => $rolecount,
    'Number of users in each role' => $rolecounts,
  );
  return $return;
}